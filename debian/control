Source: libio-pipely-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Florian Schlichting <fsfs@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: perl
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libio-pipely-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libio-pipely-perl.git
Homepage: https://metacpan.org/release/IO-Pipely
Rules-Requires-Root: no

Package: libio-pipely-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends}
Description: portable way to create pipe() or pipe-like handles
 IO::Pipely provides a couple functions to portably create one- and
 two-way pipes and pipe-like socket pairs. It acknowledges and works
 around known platform issues so you don't have to.
 .
 IO::Pipely currently understands pipe(), UNIX-domain socketpair() and
 regular IPv4 localhost sockets. It will use different kinds of pipes or
 sockets depending on the operating system's capabilities and the number
 of directions requested. The autodetection may be overridden by
 specifying a particular pipe type.
 .
 IO::Pipely is a spin-off of the POE project's portable pipes. Earlier
 versions of the code have been tested and used in production systems for
 over a decade.
